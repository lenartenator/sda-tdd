package pl.sda.tdd;
/*Napisz program, który będzie reprezentował koszyk sklepowy do którego klient może umieścić zamawiane produkty - książki.
Utwórz klasę reprezentującą Książkę o nazwie Book. Przyjmijmy, że książka na początku posiada  tylko 3 parametry:
1. Tytuł  2. Autor  3. Cena
Dodaj także klasę reprezentującą koszyk o nazwie Basket.
Klasa ta musi posiadać listę pozwalającą przechowywać obiekty klasy Book oraz wykonywać różne dodatkowe operacje.
Funkcjonalności:
1. Tworzenie obiektów klasy Book podając tytuł, autora i cenę książki
2. Możliwość zmiany ceny książki (np. 2 sposoby: podając nową kwotę lub % o ile należy zmienić cenę)
3. Pobranie listy wszystkich książek dodanych do koszyka
4. Dodawanie / usuwanie książek z koszyka
5. Czyszczenie koszyka
6. Suma wszystkich produktów znajdujących się w koszyku
7. Usuwanie z koszyka na podstawie np nazwy
8. Zliczanie ilości książek w koszyku danego tytułu.*/
import lombok.Builder;
import lombok.Data; //konstruktor, gettety,settery, to stringi, hasze itd. ma w sobie

import java.math.BigDecimal;

@Data
@Builder
public class Book {
    private String author;
    private String title;
    private BigDecimal price;

    public void changePrice(BigDecimal newPrice) {
        setPrice(newPrice);
    }
    public void changePriceByPercentage(int percentage) {
        if (percentage<=0) {
            throw new IllegalArgumentException("Procent musi być dodatni");
        }
        else setPrice(BigDecimal.valueOf(percentage)
                .multiply(this.price)
                .divide(new BigDecimal(100)));
    }

    }


// bez Lomboka tradycyjnie trzeba by zrobić wszystko tradycjnie, czyli poniższe:

    /*
    public class Book {
    private String title;
    private String author;
    private double price;
    private int qty =0;

    public Book(){
    }
    public Book(String name, String author,double price){
        this.title=name;
        this.author=author;
        setPrice(price);
    }

    public Book(String name, String author,double price, int qty){
        this.title=name;
        this.author=author;
        setPrice(price);
        setQty(qty);
    }

    public String getName(){
        return this.title;
    }
    public String getAuthor(){
        return this.author;
    }
    public double getPrice(){
        return this.price;
    }
    public void setPrice(double price){
        this.price=price;
    }
    public int getQty(){
        return qty;
    }
    public void setQty(int qty){
        this.qty=qty;
    }

    public String toString(){
        return "Book[name="+title+" "+getAuthor().toString()+" ,price="+price+" ,qty="+qty+"]";
    }*/

