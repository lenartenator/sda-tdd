package pl.sda.tdd;

import java.util.Arrays;
import java.util.Random;

public class MedianaTablicy {

    public Integer find (int [] tab) {

    if (tab == null || tab.length ==0) {
        return null;
    }
    if(tab.length % 2 == 0) {
        return tab [(tab.length - 1) / 2];
    }

    return tab[tab.length /2];
    }

    public static double mediana(int[] tablica) {
        double mediana=0;
        double srednia = 0.0;
        //Arrays.sort(tablica);

        if (tablica.length % 2 == 0 )
        {
            srednia = tablica[tablica.length/2] + tablica[(tablica.length/2)-1];

            mediana = srednia/2.0;

        }
        else // jeżeli tablica nieparzysta, mediana to średnia wartość dwóch środkowych elementów
        {
            mediana = tablica[tablica.length/2];
        }

        return mediana;
    }


    public static void main(String[] args)
    {
        int w = 10;
        int[] tab = new int[w];
        Random r = new Random();

        for (int i = 0 ; i < tab.length ; i++)
        {
            tab[i] = r.nextInt(9) + 1;
        }
        System.out.println(Arrays.toString(tab));
        System.out.println(mediana(tab));
    }
}
