package pl.sda.tdd;

public class Calc {
    public static int Sum(String input) {
        if (input.equals("")) {
            return 0;

        } else if (input.startsWith("//")) {
            char customSeparator = input.charAt(2);
            String cutInput = input.substring(4);
            String [] tokens = cutInput.split(",|\n|" + customSeparator);
            Integer sum = 0;
            for (String token : tokens
                    ) {
                int lesserThan= Integer.parseInt(token);
                if (lesserThan <=1000)
                    sum = sum + lesserThan;
            }
            return sum;


        } else if (input.contains("-")) {
            throw new IllegalArgumentException("Cannot accept negative values");
        }

        else {
            String[] tokens = input.split(",|\n");
            Integer sum = 0;
            for (String token : tokens
                    ) {
                int lesserThan= Integer.parseInt(token);
                if (lesserThan <=1000)
                    sum = sum + lesserThan;
            }
            return sum;
        }


    }
}
