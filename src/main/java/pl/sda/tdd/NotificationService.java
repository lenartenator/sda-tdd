package pl.sda.tdd;

public class NotificationService {
    private EmailService emailService;
    private PigeonService pigeonService;

    public void sendNotification() {
        if (emailService.isAvailable()) {
            emailService.sendEmail("Wysłano e-mail");
        } else if (pigeonService.isAvailable()) {
            pigeonService.sendMessage("Wysłano gołębia");
        } else {
            throw new IllegalArgumentException("Serwisy niedostępne"); //typ błędu jest akurat pierwszy z brzegu
        }
    }
}


