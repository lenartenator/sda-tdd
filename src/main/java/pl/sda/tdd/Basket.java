package pl.sda.tdd;
import lombok.Builder;
import lombok.Data;
import lombok.Singular;
import java.util.List;

public class Basket {
    @Singular           //to jest lombokowa adnotacja dla kolekcji zawierająca jej funkcjonalności, żeby nie pisać wszystkiego
                        // dodaje elementy min. oraz też sprawdza, czy nie jest nullem i ją w razie potrzeby inicjuje
    private List<Book> books;

}
