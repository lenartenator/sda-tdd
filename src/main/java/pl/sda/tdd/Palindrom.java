package pl.sda.tdd;

import java.security.InvalidParameterException;

public class Palindrom {
    public static void main(String[] args) {

        String palindrom = "kajak";
        System.out.println(isPalindrom(palindrom));
    }

    public static boolean isPalindrom(String str){

        if (str == null) {
            throw new InvalidParameterException("Achtung Minen");
        }

        int lewy = 0;
        int prawy = str.length()-1;

        while(lewy < prawy){

            if(str.charAt(lewy) != str.charAt(prawy)){
                return false;
            }
            lewy++;
            prawy--;
        }
        return true;


    }
}
