package pl.sda.tdd;

import org.junit.Before;
import org.junit.Test;

import static junit.framework.TestCase.assertTrue;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNull;

public class PalindromTest {
    private Palindrom palindrom;

    @Before
    public void setup () {
        this.palindrom = new Palindrom();
    }
    @Test
    public void shouldReturnNullWhenEmpty () {

            String testowy= "asdfasdfasdffa";
            assertFalse (palindrom.equals(testowy));
        }
    @Test
    public void shouldReturnNullWhenNull () {

        assertNull(palindrom.isPalindrom(null));
    }
    @Test
    public void shouldHaveKnownBetter () {
        assertTrue(palindrom.isPalindrom("kajak"));
    }

}
