package pl.sda.tdd;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertSame;
import java.math.BigDecimal;

@RunWith(MockitoJUnitRunner.class)

public class BookTest {

    private final String BOOK_AUTHOR = "Henryk Sienkiewicz";
    private final String BOOK_TITLE = "W PUSTYNI I W PUSZCZY";
    private final BigDecimal BOOK_PRICE = new BigDecimal(30);

    private Book book;

    @Rule
    public ExpectedException expectedException = ExpectedException.none();

    @Before
    public void SetUp() {
        this.book = Book.builder()
                .author(BOOK_AUTHOR)
                .title(BOOK_TITLE)
                .price(BOOK_PRICE)
                .build();
    }
    @Test
    public void shouldCreateBook() {
        assertEquals(BOOK_AUTHOR, book.getAuthor());
        assertEquals(BOOK_TITLE, book.getTitle());
        assertSame(BOOK_PRICE, book.getPrice());
    }
    @Test
    public void shouldChangePrice() {
        BigDecimal newPrice = new BigDecimal(50);
        book.changePrice(newPrice);
        assertEquals(newPrice, book.getPrice());
    }
    @Test
    public void shouldChangePriceByPercentage() {
        BigDecimal percentage = new BigDecimal(50);
        book.changePriceByPercentage(percentage.intValue());
        assertEquals(BOOK_PRICE.multiply(percentage).divide(new BigDecimal(100)), book.getPrice());
    }
    @Test
    public void shouldChangePriceException() {
        expectedException.expect(IllegalArgumentException.class);
        expectedException.expectMessage("Procent musi być dodatni");
        book.changePriceByPercentage(0);
    }
}
