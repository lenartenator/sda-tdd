package pl.sda.tdd;

import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

public class MedianaTablicyTest {
    private MedianaTablicy medianaTablicy;

    @Before
    public void setup () {
        this.medianaTablicy = new MedianaTablicy();
    }
@Test
    public void shouldReturnNullWhenEmpty () {
        int [] emptyArray = new int [0];
        assertNull (medianaTablicy.find(emptyArray));
}
    @Test
    public void shouldReturnNullWhenNull () {
        assertNull(medianaTablicy.find(null));
    }
    @Test
    public void shouldReturnElementWhenEven () {
        int[] evenArray = {1,2,3,4,5,6};
        assertEquals(Integer.valueOf(3), medianaTablicy.find(evenArray));
    }
    @Test
    public void shouldReturnElementWhenOdd () {
        int[] oddArray = {1, 2, 3, 4, 5, 6};
        assertEquals(Integer.valueOf(3), medianaTablicy.find(oddArray));
    }

    }
