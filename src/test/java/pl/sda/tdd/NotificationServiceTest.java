package pl.sda.tdd;

import javafx.beans.binding.BooleanExpression;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.InjectMocks;
import org.mockito.junit.MockitoJUnitRunner;
import static org.mockito.Mockito.when;
import static org.mockito.Mockito.verify;
import org.junit.rules.ExpectedException;

@RunWith(MockitoJUnitRunner.class)
public class NotificationServiceTest {

    @Mock
    private EmailService emailService;

    @Mock
    private PigeonService pigeonService;

    @InjectMocks
    // musi być Inject, musi wstrzyknąć te obiekty bo inaczej nie będzie żadnej interakcji z mockami
    private NotificationService notificationService;

    @Rule
    public ExpectedException expectedException = ExpectedException.none();

    @Test
    public void shouldSendEmailWhenEmailServiceIsAvailable() {
        //given
        when(emailService.isAvailable()).thenReturn(true);
        //when
        notificationService.sendNotification();
        //then
        verify(emailService).sendEmail("Wysłano e-mail");
    }

    @Test
    public void shouldSendMessageWhenPigeonServiceIsAvailable() {
        //given
        when(emailService.isAvailable()).thenReturn(false);
        when(pigeonService.isAvailable()).thenReturn(true);
        //when
        notificationService.sendNotification();
        //then
        verify(pigeonService).sendMessage("Wysłano gołębia");
    }

    @Test
    public void shouldServicesBeUnavailable() {
        //given
        when(emailService.isAvailable()).thenReturn(false);
        when(pigeonService.isAvailable()).thenReturn(false);
        //then
        expectedException.expect(IllegalArgumentException.class);
        expectedException.expectMessage("Serwisy niedostępne");
        notificationService.sendNotification();
        }
    }

