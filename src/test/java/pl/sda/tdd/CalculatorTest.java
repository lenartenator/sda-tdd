package pl.sda.tdd;

import junitparams.JUnitParamsRunner;
import junitparams.Parameters;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;

import static org.junit.Assert.assertEquals;

@RunWith(JUnitParamsRunner.class)

public class CalculatorTest {

    private Calculator calculator;

    @Rule
    public ExpectedException expectedException = ExpectedException.none();

    @Before
    public void setup () {
        this.calculator = new Calculator();
    }

    @Test
    @Parameters({
            "12,4,3",
            "16,4,4"
    })
    public void shouldDivideTwoNumbers (Integer a, Integer b, Integer expected) {
        Integer result1 = calculator.division(a, b);
       // int result2 = calculator.division(a, b);
        assertEquals(expected, result1);
       // assertEquals(4, result2);
    }
    @Test
    public void shouldAddTwoNumbers () {
        Integer a = 2;
        Integer b = 3;
        assertEquals(5, calculator.add(a,b));
    }
    @Test
    public void shouldNullPointerException(){
        Integer a = 2;
        expectedException.expect(NullPointerException.class);
        expectedException.expectMessage("Parametr jest null");

        calculator.add(a, null);
    }
    @Test
    public void shouldNullPointerExceptionInDivide(){
        Integer a = 2;
        expectedException.expect(NullPointerException.class);
        expectedException.expectMessage("Parametr w dzieleniu jest null");

        calculator.divide(null, a);
    }
}
