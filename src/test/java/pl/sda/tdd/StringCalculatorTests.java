package pl.sda.tdd;
import org.junit.Rule;
import org.junit.Test;
import org.junit.Assert;
import org.junit.rules.ExpectedException;
import static org.junit.Assert.assertEquals;

public class StringCalculatorTests {

    @Rule
    public ExpectedException expectedException = ExpectedException.none();

    @Test
    public void empty_input_returns_zero() {
        String input = "";
        int result = Calc.Sum(input);
        Assert.assertEquals(0, result);
    }

    @Test
    public void one_number_in_imput_returns_number() {
        String input = "1";
        int result = Calc.Sum(input);
        Assert.assertEquals(1, result);
    }
    @Test
    public void two_numbers_in_imput_returns_sum() {
        String input = "1,2";
        int result = Calc.Sum(input);
        Assert.assertEquals(3, result);
    }
    @Test
    public void multiple_numbers_in_imput_returns_sum() {
        String input = "1,2,5,10";
        int result = Calc.Sum(input);
        Assert.assertEquals(18, result);
    }
    @Test
    public void any_numbers_in_imput_returns_sum() {
        String input = "1,2,5,10,20";
        int result = Calc.Sum(input);
        Assert.assertEquals(38, result);
    }
    @Test
    public void new_line_in_imput_returns_sum() {
        String input = "1\n2,3";
        int result = Calc.Sum(input);
        Assert.assertEquals(6, result);
    }
    @Test
    public void custom_separtor_in_imput_returns_sum() {
        String input = "//;\n1;2;3";
        int result = Calc.Sum(input);
        Assert.assertEquals(6, result);
    }
    @Test
    public void negative_in_imput_returns_exception() {
        expectedException.expect(IllegalArgumentException.class);
        expectedException.expectMessage("Cannot accept negative values");
        String input = "1,2,-5,8";
        int result = Calc.Sum(input);
    }
    @Test
    public void ignore_over1000_in_imput_returns_sum() {
        String input = "1,2,3,1001";
        int result = Calc.Sum(input);
        Assert.assertEquals(6, result);
    }
}
